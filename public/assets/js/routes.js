/**
 |---------------------------------
 | Name Space Routes
 |---------------------------------
 |
 | This page registers the routes that determine where all links go. It is
 | important to have one central area where all routes lead so that when you
 | change them here, they change everywhere. You won't have to worry about
 | updating every single page.
 |
 | @note keep each section in alphabetical order to be able to quickly locate each route.
 |
 | @author Felipe Tadeo
 */

ROUTES = {

    /**
     * Pages links
     */
    "about_us": "/aboutus/aboutus.html",
    "home": "/",
    "immune_system" : "/ImmuneSystem/immune.html",
    "joyannce": "/", // Changed to index because if we missed one, we'd rather have them come back home than to 404
    "news" : "/News/news.html",
    "privacy_policy" : "/customer_service/privacy_policy.html",
    "return_policy" : "/customer_service/return_policy.html",
    "terms_conditions" : "/customer_service/terms_conditions.html",

    /**
     * Product links
     */
    "algazim": "/Algazim/algazim.html",
    "ala": "/ALA/alpha_lipoic_acid.html",
    "betaglucan": "/betaglucan/betaglucan.html",
    "colloidal": "/ColloidalSilver/colloidal_silver.html",
    "colosan": "/Colosan/colosan.html",
    "healthlibrary": "/HealthLibrary/Library.html",
    "hepasan": "/Hepasan/hepasan.html",
    "homozon": "/Homozon/homozon.html",
    "ketsumeisei": "/Ketsumeisei/ketsumeisei.html",
    "mincol": "/MinCol/mincol.html",
    "oxyearth": "/OxyEarth/OxyEarth.html",
    "oxyflush": "/OxyFlush/oxyflush.html",
    "oxylift": "/OxyLift/oxylift.html",
    "superoxyflush": "/SuperOxyFlush/super_oxyflush.html",
    "vitalzym": "/Vitalzym/vitalzym.html",

    /**
     * Article links
     */
    "a_j" : "/News/articles/beta_glucan_news/a_j.html",
    "beta-1_3-d" : "/News/articles/Beta-1_3-D_Glucan_Immediate_Press_Release/betapressrelease.html",
    "msm_news" : "/News/articles/msm/msm.html",
    "wash" : "/News/articles/wash/wash.html",

    /**
     * Social media links
     */
     "facebook": "https://www.facebook.com/HealthSprings.net",
     "google": "https://plus.google.com/b/100881494866733725452/100881494866733725452/about/p/pub",

     /**
      * ASecureCart links
      */
      "asecurecart" : "http://www.asecurecart.net",
      "checkout": "http://www.asecurecart.net/server/cart.aspx/healthsprings",
      "view_cart": "http://www.asecurecart.net/server/cart.aspx/healthsprings",

      /**
       * Other External links
       */
       "jouce_etsy" : "https://www.etsy.com/shop/joyannce"

       /**
        * NOTE: All refrences should end with a comma EXCEPT for the last one.
        */

}
