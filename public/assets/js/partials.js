/**
 |---------------------------------
 | Partial name spaces
 |---------------------------------
 |
 | This page is the default directory for named partials.  This page should consist
 | of reusable partials. You can change it here, so that you don't have to change
 | it in every other page.
 |
 | @author Felipe Tadeo
 */

PARTIALS = {
    /**
     * Templates
     */
    "template_blank": "/public/templates/blank.html",
    "template_main": "/public/templates/main.html",
    "template_product": "/public/templates/product.html",

    /**
     * Components
     */
    "component_banner": "/public/components/banner/banner_plain.html",
    "component_copyright" : "/public/components/footer/copyright.html",
    "component_disclaimer": "/public/components/disclaimer/disclaimer.html",
    "component_footer": "/public/components/footer/footer.html",

    "component_form-body" : "/public/components/form/form_body.html",
    "component_form-body-enzyme" : "/public/components/form/form_body-enzymecd.html",
    "component_form-footer": "/public/components/form/form_footer.html",

    "component_navbar": "/public/components/navbar/navbar.html",
    "component_poster": "/public/components/poster/empty_poster.html",
    "component_rightsidebar": "/public/components/rightsidebar/rightsidebar_plain.html",
    "component_secondarynavbar": "/public/components/secondarynavbar/secondarynavbar.html",

    /**
     * widgets
     */
     "widget_facebook-comments" : "/public/widgets/facebookcomments.html",
     "widget_facebook-sdk" : "/public/widgets/facebookjssdk.html",
     "widget_facebook-page" : "/public/widgets/facebookpage.html",
     "widget_logo" :"/public/widgets/logo.html",
     "widget_out-of-stock" : "/public/widgets/outofstock.html",
     "widget_share" : "/public/widgets/share.html",
     "widget_shipping": "/public/widgets/shipping.html",
     "widget_veggie-capsules" : "/public/widgets/veggiecapsules.html"
}
