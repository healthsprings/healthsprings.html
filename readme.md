# How To Use Your Website

This document outlines how to use components in healthsprings.net website. It will provide an explanation and an example on how to use components found through out the website.

-------------

## Built On Bootstrap 3.3.7

At the very bones of this application is Bootstrap. Bootstrap makes front-end web development faster and easier. It's made for folks of all skill levels, devices of all shapes, and projects of all sizes.

Bootstrap has a variety of features that can all be used in healthsprings.net. Here are just some of the items. You can learn more at [getbootstrap.com](http://getbootstrap.com).

### Inline text elements

#### Underlined text

To underline text use the `<u>` tag.

This line of text will render as underlined
```
<u>This line of text will render as underlined</u>
```

Make use of HTML's default emphasis tags with lightweight styles.

#### Small text

For de-emphasizing inline or blocks of text, use the `<small>` tag to set text at 85% the size of the parent. Heading elements receive their own font-size for nested `<small>` elements.

You may alternatively use an inline element with `.small` in place of any `<small>`.

This line of text is meant to be treated as fine print.
```
<small>This line of text is meant to be treated as fine print.</small>
```

#### Bold

For emphasizing a snippet of text with a heavier font-weight.

The following snippet of text is rendered as bold text.
```
<strong>rendered as bold text</strong>
```

#### Italics

For emphasizing a snippet of text with italics.

The following snippet of text is rendered as italicized text.
```
<em>rendered as italicized text</em>
```

#### Alternate elements
Feel free to use `<b>` and `<i>` in HTML5. `<b>` is meant to highlight words or phrases without conveying additional importance while `<i>` is mostly for voice, technical terms, etc.

### Blockquotes
For quoting blocks of content from another source within your document.

#### Default blockquote
Wrap `<blockquote>` around any HTML as the quote. For straight quotes, we recommend a `<p>`.

```
<blockquote>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
  </p>
</blockquote>
```

#### Blockquote options
Style and content changes for simple variations on a standard `<blockquote>`.

##### Naming a source

Add a `<footer>` for identifying the source. Wrap the name of the source work in `<cite>`.


```
<blockquote>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
  </p>
  <footer>
    Someone famous in <cite title="Source Title">Source Title</cite>
  </footer>
</blockquote>
```

##### Alternate displays

Add `.blockquote-reverse` for a blockquote with right-aligned content.

```
<blockquote class="blockquote-reverse">
  ...
</blockquote>
```

-----------

## File Structure

The file structure for the website has not changed much. There are a few new folders, but all products have stayed in the root directory.

### Dev Folder

`/dev` contains all development related files and should not be edited. You may look, but shouldn't touch. In fact, if you do change something in it, it won't do anything unless you have Gulp running.

This is strictly for development and should not be modified at all.

### Public Folder

`/public` contains all items that are shared through out the website. This includes style sheets, JavaScript files, images and partial templates. We'll reference this folder often through out this tutorial.

### Gulpfile.js

`/gulpfile.js` is another development only file that should not be modified. It is used when developing the project and is responsible for telling the contents of `/dev` what to do.

### Env.js

`env.js`, which stands for environment, holds all the values that change depending on which environment we're working in.

For example, this project was developed on a local machine, so the URL would not be the same as it would be in say, the actual server.

#### Environment Variables

The environment file contains what are known as Global variables. Which means that they may be used through out the project in Javascript.

##### URL variable

The `URL` variable is the url, which needs to include the protocol `http://` or `https://`, that the current project is hosted on.

If it were being hosted on a local machine then it would look something like this:

```
URL = "http://sandbox.healthsprings.com";
```

If it were a beta website, then something like this:

```
URL = "http://beta.healthsprings.com";
```

### package.json

This file is for developers only. It is responsible for maintaining a list of dependencies that your website needs to be developed with.

### DevReadMe.md

This is the readme file for development purposes. Refer a developer to this file to understand how to develop for your website.


--------------------

## Adding a New Page

Creating a new page is simple, and now there are a few options for themes with that page.

### Basic Page Folder Structure

At the very least, you need two files inside a new folder: `content.html` and `yourpage.html`.

The first page that we'll call `yourpage` for demonstration purposes is the page that will get loaded initially.

We'll create a folder called `newpage`, and inside that folder we'll place `yourpage.html`:

```
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- Ensure proper rendering and touchzooming for mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="/public/assets/img/favicon.png">

    <!-- Edit Title -->
    <title>Health Springs | New Page</title>

    <meta name="description" content="My New Page Description">
    <meta name="author" content="John Doe">

    <link rel="stylesheet" href="/public/assets/css/app.css">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->

    <script src="/env.js"></script>
    <script src="/public/assets/js/partials.js"></script>
    <script src="/public/assets/js/routes.js"></script>
    <script src="/public/assets/js/main.js"></script>
    <script src="https://use.fontawesome.com/76eb79f76b.js"></script>

    <script>
    /**
     * Define the unique partials for this page
     */
    TEMPLATE_PARTIALS = {
        content: "/newpage/content.html"

    }
    </script>
</head>

<body>

    <!-- Start Main Wrapper -->

    <div class="container accesory_image-half_leaf" id="main_wrapper" data-partial data-partial-name="template_product">
    </div>

    <!-- End Main Wrapper -->

    <script>
        $(function() {

          initPage();

        })
    </script>


</body>

</html>

```

There are three main sections we need to edit. Lets take a look at the first one:

### Title and Meta Tags

```
<!-- Edit Title -->
 <title>Health Springs | New Page</title>

 <meta name="description" content="My New Page Description">
 <meta name="author" content="John Doe">
```

Here we edit the `<title></title>`, this is the title that will appear on the browser tab. In our case, we've named it: "Health Springs | New Page".

The `<meta>` tags are used to give the search engines and other crawlers a description of the page. Here we have a `description`, which we've pouplated with "My New Page Description" and `author`, who we'll say is "John Doe".

### Template Partials

The template partials section, at the very minimum, must have the `content.html` reference. As you can see, we've used our `newpage/content.html` that should be located in the same section.

```
<script>
   /**
    * Define the unique partials for this page
    */
   TEMPLATE_PARTIALS = {
       content: "/newpage/content.html"

   }
</script>
```

Each template comes with a default component known as a partial (see [Partials](#partials)), whether it's the Navigation bar or the sidebar. Sometimes we don't want to use the default component, instead we may have created a custom component based whatever page we make.

To learn more on how to override, please see [Overriding Template Defaults](#overridingtemplatedefaults).


---------------------

## Init Page

Whenever you use any feature that is Javascript based (anything past this point), you'll need to make sure that this piece of code is at the bottom of every page.

```
<script>
    $(function() {
        initPage();
    })
</script>
```

This piece of code will initiate any Javascript features.

-------------

## Partials

Sometimes we have components of a web page that appear through out the website over and over again. A great example of this would be the navigation bar. If we were to create a new page, we would copy and paste over the navigation bar to the new page. If we have yet another new page, we would do the same thing and so on for other similar processes.

*What if you wanted to change a link?* We would then have to go in and change the link to **EVERY SINGLE PAGE** you added a navigation bar to. Not fun. Not only that but what if you miss one? This is where partials solves the problem.

Instead of copying and pasting a block of HTML we simply reference it and the partial gets loaded in. Now, you only have to change one code and the changes get propagated through out the pages.

### Loading a partial form a src

Suppose we want to load the navbar in our new page. It is as simple as this:

```
  <div data-partial data-partial-src="/public/components/navbar/navbar.html"> </div>
```

`data-partial` must be included in the first tag of the parent container. This lets the website know it is a partial element.

`data-partial-src` contains the relative directory (path from the main folder your project is located in) path to the content that is to be injected in.

When the page is rendered, the contents of `/public/components/navbar/navbar.html` will be injected inside the `<div>` element.

If it's a component we plan on using over and over again, which a navigation bar is, we don't want to always type out the full path. That's where names come in.

### Loading a partial from a name

Using names instead of src has two advantages:

1) You don't have to write out the full path to the source file.
2) If you decide to change the source file, you don't need to go in and update all references, just the directory.

First you need to register the partial in: `/public/assets/js/partials.js`

It will look something like so:

```
PARTIALS = {
    /**
     * Templates
     */
    "template_blank": "/public/templates/blank.html",
    "template_blank": "/public/templates/blank.html",
    "template_main": "/public/templates/main.html",
    "template_product": "/public/templates/product.html"
  }
```

***Important*** All items are separated by comma. The last item should NOT have a comma.

As you can see, our templates are already there. `template_blank` points to `/public/templates/blank.html`. So lets add navbar to our components.

```
PARTIALS = {
    /**
     * Templates
     */
    "template_blank": "/public/templates/blank.html",
    "template_blank": "/public/templates/blank.html",
    "template_main": "/public/templates/main.html",
    "template_product": "/public/templates/product.html",

    /**
     * Components
     */
     "component_navbar": "/public/components/navbar/navbar.html"
  }
```

Now we have the navbar registered as `component_navbar` which points to `/public/components/navbar/navbar.html`. We can now use it as a name like so:

```
<div data-partial data-partial-name="component_navbar"></div>
```

We've included `data-partial` to indicate that it is a partial, and `data-partial-name="component_navbar"` lets it know to use the `component_navbar`, which it will grab from the `partials.js` file.

*Tip: You'll notice that each name is prefixed with `template_` or `component_` or `widget_`. There is no technical requirement to do this, but we do this to remind us what the role of each partial is. They are also in alphabetical order to find them faster.*

The final output will be rendered like so:

```
<div data-partial data-partial-name="component_navbar">
  <navbar>
    ...
  </navbar>
</div>
```

### Mustache Variables

Having templates and loading partials significantly reduces the amount of code we have to manage. *But what if we want to recycle the same 'frame' but with different values?* That's where the Mustache Variables come in.

Suppose we have a redundant frame where only some values change, but for the most part the code stays the the same. We can copy and paste it but over time the form can lose it's structure, or if we want to update the form's look, we'll have to go through every product and do so!

Why not only have the values that are unique to each form change, while the frame stays the same. `data-partial-values` solves this problem.

Lets take a look at the product form.

```
<form class="form-inline" method=post action="http://www.asecurecart.net/server/cart.aspx/healthsprings">

         <input type="hidden" name="Describe" VALUE="HSHPSN60">
         <input type="hidden" name="Price" value="18.95">
         <input type="hidden" name="Show" value="http://www.healthsprings.net/Hepasan/hepa-colosan.html">
         <input type="hidden" name="DiscStr" value="1,100,18.95">
         <input type="hidden" name="DiscItem" value="N">
         <input type="hidden" name="ID" VALUE="Hepasan 60 caps">
         <input type="hidden" name="ReturnLink" value="http://www.healthsprings.net/Hepasan/hepasan.html">


         <div class="input-group">
              <div class="input-group-addon">Qty</div>
              <input type="text" class="form-control" name="Qty" size=3 value="1">
         </div>


         <br />

         <button type="submit" class="btn btn-primary btn-block btn-lg"> <i class="fa fa-cart-plus" aria-hidden="true"></i> Add To Cart</button>

</form>
```

Most products have this basic form structure. A set of hidden inputs, followed by the Quanity text field and concluded with the submit button.

Lets create a template out of this and register it to our `partials.js` file as we did with our previous example. We'll name it `component_form-body`.

Now, that we've created the template and placed it in:

`public/components/form/form_body.html`


Here's what a Mustache Variable looks like: `{{variableName}}`

Like an HTML tag starts with `<`, our mustache variable starts with `{{` and ends with `}}`. This lets your website know that it is a variable. Then, with no space inbetween the braces and the start and end of the variable, enter any variable name. In our example, we've named it `variableName`. **No spaces are allowed**.

We can replace the values that we want to arbitrarily change with Mustache Variables in our newly created `form_body` template.


```
<form class="form-inline" method=post action="http://www.asecurecart.net/server/cart.aspx/healthsprings">

         <input type="hidden" name="Describe" VALUE="{{describe}}">
         <input type="hidden" name="Price" value="{{price}}">
         <input type="hidden" name="Show" value="{{showURL}}">
         <input type="hidden" name="DiscStr" value="{{discStr}}">
         <input type="hidden" name="DiscItem" value="{{discItem}}">
         <input type="hidden" name="ID" VALUE="{{id}}">
         <input type="hidden" name="ReturnLink" value="{{returnLink}}">


         <div class="input-group">
              <div class="input-group-addon">Qty</div>
              <input type="text" class="form-control" name="Qty" size=3 value="1">
         </div>


         <br />

         <button type="submit" class="btn btn-primary btn-block btn-lg"> <i class="fa fa-cart-plus" aria-hidden="true"></i> Add To Cart</button>

</form>
```

Now that we have a template ready to be used, we can now build build the wrapper for it where we want the template to be injected:


```
<div data-partial data-partial-name="component_form-body" data-partial-values>
	"describe" : "HSHPSN60"
	"price" : "18.95"
	"route_showURL" : "betaglucan"

	"discStr" : "1,100,18.95"
	"discItem" : "N"
	"id" : "Hepasan 60 caps"
	"route_returnLink" : "betaglucan"
</div>
```
**Note: we are including a new data attribute `data-partial-values`** this lets the website know to look for key valued pairs inside the html element.

The syntax is pretty simple. You have two items: a key and a value seporated by a colon. The key, which is the first item wrapped in quotations, is the name of the variable we want to replace. The colon serves as an '=' sign and points to the second item which is the value that we want to replace it with.

#### Syntax:

 - You must wrap both key and value with quotation marks or an error will be thrown.
 - They must be seporated by a colon.
 - Only one key pair per line.
 - Do not break into a new line in the middle of a value, instead use `\n` where a new line should start.
 - You can have empty lines in between each new key valued pair as shown in the example above in between `"showURL"` and `"discStr"` to make it easier to read.


### Overriding Template Defaults



------------

## Routes

Routes allow you to use names instead of urls or paths for links. By doing so, we reduce the amount of links we have to change to just one centralized location.

### Registering Routes

First we register a route inside the `routes.js` located in:

`/public/assets/js/routes.js`

```
ROUTES = {

    /**
     * Pages links
     */
    "about_us": "/aboutus/aboutus.html",
    "home": "/",
    "immune_system" : "/ImmuneSystem/immune.html",
    "joyannce": "/artwork/joyannce.html",
    "news" : "/News/news.html",
    ...
  }
```

Using the same convention as registering a partial template, you pick a name and the relevant path to the file.

You may also register URLs as we have already with social links:

```
...
/**
 * Social media links
 */
 "facebook": "https://www.facebook.com/HealthSprings.net",
 "google": "https://plus.google.com/b/100881494866733725452/100881494866733725452/about/p/pub",

...
```

**Note** when registering a URL, you must include the protocol: `http://` or `https://` depending what website you're pointing to.

### Using Routes

Once registered, a route can be used by name in a link like so:

```
  <a data-route data-route-name="facebook">FaceBook</a>
```

The output will look like this:

```
<a data-route data-route-name="facebook" href="https://www.facebook.com/HealthSprings.net">FaceBook</a>
```

Now, if say, your Facebook page changes, you don't need to update all the links, just the route inside `/public/assets/js/routes.js`.

### Routes In Mustache Variables

You may also routes inside a mustache variable by following a simple syntax. First, the name of the mustache variable must start with `route_`. Then the value may be the name of the route:

Inside a template:
```
  ...
    {{route_product}}
  ...
```

When declaring the values:
```
  "route_product" : "colosan"
```

The output will yield the URL to the product "colosan".

------------

## Forms

Forms have been reduced to a simple partial with mustache variables. There are two form templates, one with an enzyme CD option and one without. These templates are located in:

`/public/assets/components/form`

### Form Partials

Form partials are not much different from regular partials. You register them and initiate them as you would any other partial.

```
  <div data-partial data-partial-name="component_form-body" data-partial-values>
    ...
  </div>
```

The one place they do differ is the fact that they have inputs which in some cases, not all forms will share the same inputs so using mustache variables would not help us.

### Redirect Link

The only regular mustache variable in a form is the `"route_redirectLink"` which tells the form what page to go back to when the customer clicks the "Continue Shopping" button. **This is required**.

```
  <div data-partial data-partial-name="component_form-body" data-partial-values>
    "route_redirectLink" : "colosan"
    ...
  </div>
```

### Form Inputs

For the rest of the form inputs, we'll use a list of variables. The name of this list is called `"inputs_hidden":`.

As shown in the example below, the name of the list ends with a `:`. Then each item in the list is a normal mustache variable followed by a `,`. *The last item in the list does not have a comma*. This is how the website knows that it is the end of the list.

```
<div data-partial data-partial-name="component_form-body" data-partial-values>
    "route_redirectLink" : "hapasan"

    "inputs_hidden":
        "Price" : "18.95",
        "DiscStr" : "1,100,18.95",
        "DiscItem" : "N",
        "ID" : "Hepasan 60 caps"
</div>
```

The output will look like this:

```
<div data-partial="" data-partial-name="component_form-body">
<!--
### Default form template for form_service
Form service requires a {{hidden_input}} variable to be set.
-->

<div align="center">
    <form method="post" action="http://www.asecurecart.net/server/cart.aspx/healthsprings">

      <!-- This is where all the hidden inputs will go -->
      <input type="hidden" value="hapasan" name="RedirectLink">

      <input type="hidden" value="18.95" name="Price">
      <input type="hidden" value="1,100,18.95" name="DiscStr">
      <input type="hidden" value="N" name="DiscItem">
      <input type="hidden" value="Hepasan 60 caps" name="ID">

        <br>
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon">Qty.</div>
            <input type="tel" name="Qty" size="3" value="1" class="form-control text-center">
          </div>
        </div>

        <button type="submit" class="btn btn-primary btn-block btn-lg"> <i class="fa fa-cart-plus" aria-hidden="true"></i> Add This To Cart</button>
    </form>
</div>
</div>
```

We've saved ourselves from writing a lot of code!
