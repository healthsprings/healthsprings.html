/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

 // Prevent items form being cached
 $.ajaxSetup ({
     // Disable caching of AJAX responses
     cache: false
 });

loadComponents = function(data, base) {
    $.each(data, function(key, value) {
        var prevent_cache = Date.now();
        $("#" + key).load(base + value + "?ft=" + prevent_cache, function(){
          $("[data-route]").apply_routes(ROUTES);
        });
    })

}

initPage = function (){
  var overall_partials = $.extend(PARTIALS, TEMPLATE_PARTIALS);

  $("[data-partial]").apply_partials(overall_partials);

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=208646522874562";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

}
