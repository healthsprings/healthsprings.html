/**
 |---------------------------------
 | Title
 |---------------------------------
 |
 | Description
 |
 | @author Felipe Tadeo
 */

(function($) {

    var methods = {
        init: function(routes) {
            // cache elements
            var $elements = $(this);

            // loop through each element
            $elements.each(function(index) {
                var name = $(this).data("route-name");
                if (typeof routes[name] === "string" && name != "") {
                    var url = methods.getRouteURL(routes, name);
                    if (!url) {
                        alert("Failed to load route " + name)
                    } else {
                        $(this).attr("href", url);
                    }

                } else {
                    // Mark un
                }
            });

        },

        /**
         *
         */
        getRouteURL: function(routes, name) {
            var
                expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i,
                regex = new RegExp(expression);

            // check for this url
            if (name === "THIS_URL"){
              return window.location.href;
            }

            // Make sure that the value exists
            if (typeof routes[name] === "string" && name != "") {
                // Check if it's an external URL by looking for
                if (routes[name].match(regex)) {
                    return routes[name];
                } else {
                    return URL + routes[name];
                }

            } else {
                return false;
            }
        }
    }

    $.fn.apply_routes = function(routes) {
        methods.init.apply(this, arguments);
    }

    $.routeToURL = function(routes, name) {
        return methods.getRouteURL(routes, name);
    }

}(jQuery));
