/**
 |---------------------------------
 | Partial Service
 |---------------------------------
 |
 | This code is resonsible for loading all the partial components.
 |
 | @author Felipe Tadeo
 */

(function($) {
    var methods = {

        /**
         *
         */
        options: {
            selector: "partial",
            src: "partial-src",
            name: "partial-name",
            partials: {},

            // Inline options
            inlineActions: {

                /**
                 * Gets route
                 */
                route: function(key, val) {
                    return $.routeToURL(ROUTES, val);
                },

                /**
                 *
                 */
                inputs: function(key, val) {
                  var inputs = "";
                    $.each(val, function(k, v){

                      inputs = inputs + "<input type='hidden' value='"+ v +"' name='"+ k +"' />";

                    });

                    return inputs;
                }
            }
        },

        /**
         *
         */
        init: function(partials) {
            methods.options.partials = partials;
            // Cache elements
            var $elements = $(this);

            $elements.each(function(key, value) {

                var src = methods.getSrc($(this)),
                    $el = $(this);

                // Check if the element has a partial loaded already or if it's a name.
                if ($el.data("partial-loaded") != src) {
                    // console.log("Loading " + src + " into " + $el.attr("id"));

                    var inlineValues = methods.gatherInlineValues($el);
                    $el.data("partial-loaded", src);

                    methods.loadPartial(src, inlineValues, $el, function() {
                        $el.find("[data-route]").apply_routes(ROUTES);
                    });

                } else {

                }

            });

        },



        /**
         *
         */
        getSrc: function($element) {
            // Does element refrence a src by src or name?
            if (typeof $element.data(methods.options.name) == "string") {

                if (typeof methods.options.partials[$element.data(methods.options.name)] == "string") {
                    return methods.options.partials[$element.data(methods.options.name)];
                } else {
                    alert("ERROR: Could not find a partial with the name '" + $element.data(methods.options.name) + "' in partial_service.js or TEMPALTE_PARTIALS");
                }

            } else if (typeof $element.data(methods.options.src) == "string") {
                return $element.data(methods.options.src);
            } else {
                alert("WARNING: An element was set to be used as a partial but no 'data-partial-src' or 'data-partial-name' attribute could be found.")
                return false;
            }
        },

        /**
         *
         */
        loadPartial: function(src, replacementArray, element, callback) {

            $.ajax({
                    url: src,
                    dataType: "text"
                })
                .done(function(data) {

                    if (typeof replacementArray == "object") {
                        var newData = methods.handleReplacements(replacementArray, data);
                    } else {
                        newData = data;
                    }

                    element.html(newData);
                    callback();
                });
        },

        /**
         *
         */
        gatherInlineValues: function($element, override) {
            // Check if element has inline attribute
            if (typeof override === "undefined") {
                override = false;
            }
            if ($element[0].hasAttribute('data-partial-values') == false && override != true) {
                return false;
            }
            // Has the value.

            $element.removeAttr("data-partial-values");

            // Parse Text to json
           var
                text = $element.text().trim(), // Trim main Text
                textArray = text.split("\n"), // Split text by break
                jSon = {}, // Create main json object to return
                nested = {}, // temporary nested features
                nestedname = undefined,
                pnest = false;

            // Create mini json objects out of each array
            $.each(textArray, function(k, v) {
            //     // Check if nested  has started:
            //
                var
                    trimmedValue = v.trim(); // Trim value

                    if (trimmedValue.match(":$")) {
                        // has started a nested object
                        nestedname = trimmedValue.replace(/\"/g, "").replace(/:/g, "").trim();
                        pnest = true;
                        return;
                    }
                if (pnest) {

                    var vJason = JSON.parse("{" + trimmedValue.replace(/(,$)/g, "") + "}"); // Wrap it with curlies
                    nested = $.extend(nested, vJason);

                    // Check if this is the end of the nested object
                    if (!/(,$)/.test(trimmedValue)) {
                        // no comma was detected, this is the end of the nested value
                        jSon[nestedname] = nested;

                        nested = {};
                        nestedname = undefined;
                        pnest = false;

                    }

                } else {
                    var vJason = JSON.parse("{" + trimmedValue + "}"); // Wrap it with curlies
                    jSon = $.extend(jSon, vJason);
                }


                // Join it to the main json object.

            });

            return jSon;


        },

        processInlineValues: function(k, v) {

            var processed = false;
            $.each(methods.options.inlineActions, function(action, method) {
                var checkActionRegex = new RegExp("^" + action + "_");
                if (k.match(checkActionRegex) !== null) {
                    // var trimmed_value = k.replace(action, "");
                    processed = method(k, v);
                    return;
                }

            });

            return processed;


        },
        /**
         *
         */
        handleReplacements: function(data, text) {

            var returnText = text;

            $.each(data, function(k, value) {

                // Check if there is an action associated with the value
                var result = methods.processInlineValues(k, value);
                if (!result) {
                    returnText = returnText.replace("{{" + k + "}}", value);
                } else {
                    returnText = returnText.replace("{{" + k + "}}", result);
                }


            });

            return returnText;

        }

    }

    /**
     *
     */
    $.fn.apply_partials = function() {
        methods.init.apply(this, arguments);
    }

    /**
     *
     */
    $.fn.getInlineValues = function() {
        return methods.processInlineValues(this, true);
    }

}(jQuery));
